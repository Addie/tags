var gulp = require('gulp-help')(require('gulp'));
var config = require('../config.json');

// copy:assets
// Copie les assets dans public
// ----------------------------------------------------------------------------
gulp.task('copy:assets',"Copie les assets dans public", function() {
  gulp.src([
    config.paths.assets + 'project/**',
    '!' + config.paths.assets + 'project/{scss,scss/**}',
    '!' + config.paths.assets + 'project/{scripts,scripts/**}'])
    .pipe(gulp.dest(config.paths.build + 'assets'));
});

// copy:favicon
// Ajoute la favicon à la racine de public
// ----------------------------------------------------------------------------
gulp.task('copy:favicon',"Copie favicon.ico à la racine de `public", function() {
  gulp.src(config.paths.assets + 'project/favicon/favicon.ico')
    .pipe(gulp.dest(config.paths.build));
});
