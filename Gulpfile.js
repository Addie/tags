// Scampi Twig Gulpfile


// ----------------------------------------------------------------------------
// Pour connaître l'ensemble des tâches disponibles :
//
// $ gulp
//
// ----------------------------------------------------------------------------

var gulp    = require('gulp-help')(require('gulp'));

// Import des taches du repertoire tasks
var clean   = require('./tasks/clean.js');
var clean   = require('./tasks/assets.js');
var html    = require('./tasks/html.js');
var css     = require('./tasks/css.js');
var script  = require('./tasks/scripts.js');
var watch   = require('./tasks/watch.js');
var svg     = require('./tasks/svg.js');
var build   = require('./tasks/build.js');
