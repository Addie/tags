var gulp         = require('gulp-help')(require('gulp'));
var config       = require('../config.json');
var browserSync = require("browser-sync").create();

// live
// Lance l'index dans le navigateur et surveille les modifs
// ----------------------------------------------------------------------------
gulp.task('live',"Lance l'index dans le navigateur et surveille les modifs", function() {
  browserSync.init({
    server: config.paths.build
  });
  gulp.watch([
  	config.paths.pages + '**/*.twig',
  	config.paths.templates + '**/*.twig'],
  	['make:html']).on('change', browserSync.reload);
  gulp.watch([
  	config.paths.assets + 'project/scss/**/*.scss'],
  	['make:css']).on('change', browserSync.reload);
});
